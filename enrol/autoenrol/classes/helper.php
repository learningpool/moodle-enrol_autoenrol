<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * File containing helper class
 * @package    enrol_autoenrol
 * @author     Barry Oosthuizen <barry.oosthuizen@gmail.com>
 * @copyright  2016 Mind Click Ltd
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
namespace enrol_autoenrol;

/**
 * Class helper
 * @package block_stories
 */
class helper {

    const ORGANISE_BY_NONE = 0;
    const ORGANISE_BY_AUTH_METHOD = 1;
    const ORGANISE_BY_DEPARTMENT = 2;
    const ORGANISE_BY_INSTITUTION = 3;
    const ORGANISE_BY_LANGUAGE = 4;
    const ORGANISE_BY_EMAIL = 5;
    const ORGANISE_BY_COHORT_NAME = 6;

    const ENROL_ON_COURSE_VIEW = 0;
    const ENROL_ON_LOGIN = 1;

    const ENROL_ONCE = 0;
    const ENROL_ALWAYS = 1;

    const GROUPS_NONE = 0;
    const GROUPS_CREATE = 1;

    /**
     * Get a list of cohorts available to the current user
     * @param bool|true $forform
     * @return array
     * @throws \coding_exception
     * @throws \dml_exception
     */
    public static function get_cohorts_list($forform = true) {
        global $COURSE, $DB;

        $coursecontext = \context_course::instance($COURSE->id);
        $cohorts = array();
        if ($forform) {
            $cohorts = array('' => get_string('choosedots'));
        }

        list($sqlparents, $params) = $DB->get_in_or_equal($coursecontext->get_parent_context_ids());

        $sql = "SELECT id, name, idnumber, contextid
                      FROM {cohort}
                     WHERE contextid $sqlparents
                  ORDER BY name ASC, idnumber ASC";

        $rs = $DB->get_recordset_sql($sql, $params);
        foreach ($rs as $c) {
            $context = \context::instance_by_id($c->contextid);
            if (!has_capability('moodle/cohort:view', $context)) {
                continue;
            }
            $cohorts[$c->id] = format_string($c->name);
        }
        $rs->close();
        return $cohorts;
    }

    /**
     * Check whether a given user is a member of a given cohort
     * @param int $userid
     * @param int $cohortname
     * @return bool
     */
    public static function user_in_cohort($userid, $cohortname, $softmatch = false) {
        global $DB;

        $params = array('userid' => $userid);
        $where = "WHERE userid = :userid ";
        if($softmatch) {
            $searchparam = '%' . $DB->sql_like_escape($cohortname) . '%';
            $params['name'] = $searchparam;
            $where .= "AND " . $DB->sql_like('name', ':name', false);
        } else {
            $params['name'] = $cohortname;
            $where .= "AND name = :name";
        }
        $sql = "SELECT chm.id FROM {cohort_members} chm JOIN {cohort} ch ON ch.id = chm.cohortid " . $where;

        $ismember = $DB->record_exists_sql($sql, $params);
        return $ismember;
    }

}
